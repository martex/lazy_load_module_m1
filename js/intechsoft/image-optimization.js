document.observe("dom:loaded", function() {

    var imageCollection = $$('div.lazy-load .product-image img');

    function loadImages(){
        imageCollection.each(function (elem)
        {
            if (checkImageBeforeLoad(getElementOffset(elem)) && elem.className.indexOf('lazy-image') >= 0){
                imageSrc = elem.dataset.src;
                elem.src = imageSrc;
            }
        });
    }

    function checkImageBeforeLoad(elementOffset){

        return (elementOffset < screen.height);
    }

    function getElementOffset(elem){
        bodyRect = document.body.getBoundingClientRect();
        elemRect = elem.getBoundingClientRect();
        return (elemRect.top - bodyRect.top);
    }
    function insertPreloader(elem){
        elem.insert({ after: "<div class='image-preloader'></div>" });
    }
    function removePreloader(elem){
        elem.observe('load', function() {
            elem.next().remove();
        });
    }

    Event.observe(window, 'scroll', function() {
        var winPosition = document.viewport.getScrollOffsets()[1] + document.viewport.getHeight();
        $(imageCollection).each(function(elem){
            conteiner = elem;

            var elemntHeight = Element.cumulativeOffset(elem)[1] - elem.height;
           if (elemntHeight < winPosition) {

               var imageSrc = elem.dataset.src;
               if (imageSrc != elem.src){
                   insertPreloader(elem);
                   elem.next().setStyle({display: 'block'});
                   elem.src = imageSrc;
                   removePreloader(elem);
               }
           }
        })
    });

    Event.observe(window, 'resize', function() {
        loadImages();
    });

    loadImages();
});