<?php

/**
 * Class D4fs_Addtowishlistajax_Helper_Data
 */
class Intechsoft_LoadOptimization_Helper_Data extends Mage_Core_Helper_Abstract
{

    /**
     * @param string $fileName
     * @param int $width
     * @param int $height
     * @return string
     */
    public function resizeImage($fileName, $width=NULL, $height=NULL)
    {
        $folderURL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
        $imageURL = $folderURL . $fileName;

        $basePath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . $fileName;
        $newPath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . "resized" . DS . "cache". DS . $fileName;

        //if width empty then return original size image's URL
        if ($width != '') {
            //if image has already resized then just return URL
            if (file_exists($basePath) && is_file($basePath) && !file_exists($newPath)) {
                $imageObj = new Varien_Image($basePath);
                $imageObj->constrainOnly(TRUE);
                $imageObj->keepAspectRatio(FALSE);
                $imageObj->keepFrame(FALSE);
                $imageObj->resize($width, $height);
                $imageObj->save($newPath);
            }
            $resizedURL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . "resized". DS ."cache". DS . $fileName;
        } else {
            $resizedURL = $imageURL;
        }

        return $resizedURL;
    }

    /**
     * @return bool
     */
    public function isLazyLoadEnabled()
    {
        return Mage::getStoreConfigFlag('catalog/intechsoft_lazy_load/enabled');
    }

    /**
     * @return bool
     */
    public function isResizePlaceholderEnabled()
    {
        return Mage::getStoreConfigFlag('catalog/intechsoft_lazy_load/resize_enabled');
    }

    /**
     * @return array
     */

    public function getPlaceHolderSize()
    {
        return array(
                    '$width' => Mage::getStoreConfig('catalog/intechsoft_lazy_load/placeholder_width'),
                    'height' => Mage::getStoreConfig('catalog/intechsoft_lazy_load/placeholder_height'));
    }

    /**
     * @return mixed (json data)
     */
    public function getConfig()
    {
        $config = array();
        $config['timeout'] = (int)Mage::getStoreConfig('catalog/intechsoft_lazy_load/load_timeout');

        if (Mage::getStoreConfigflag('catalog/intechsoft_lazy_load/mouse_overing_enabled')){
            $config['event'] = 'mouseover';
        } else {
            $config['event'] = 'load+scroll';
        }

        $config['offset'] = (int)Mage::getStoreConfig('catalog/intechsoft_lazy_load/over_below');
        $config['placeholder'] = Mage::getBaseUrl('media'). DS .'lazyLoad'. DS .Mage::getStoreConfig('catalog/intechsoft_lazy_load/placeholder');

        if (Mage::getStoreConfigflag('catalog/intechsoft_lazy_load/ignore_hidden_images')){
            $config['loadHiddenImages'] = true;
        }
        return Mage::helper('core')->jsonEncode($config);
    }
}